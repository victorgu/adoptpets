import React from 'react';

// React.createContext() Creates 2 compnents: A provider component, and a consumer component.
// The provider is going to make everything available underneath it,
// The consumer is going to read from the provider.
const SearchContext = React.createContext({

  // Describe what the data looks like
  location: "Seatle, WA",
  animal: "",
  breed: "",
  breeds: [],
  handleAnimalChange() {},
  handleBreedChange() {},
  handleLocationChange() {},
  getBreeds() {}
});

export const Provider = SearchContext.Provider;
export const Consumer = SearchContext.Consumer;
